<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MahasiswaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/mahasiswa', [MahasiswaController::class, 'mahasiswa'])->name('data.mahasiswa');

Route::get('/createdata', [MahasiswaController::class, 'createData'])->name('data.add');

Route::post('/createdata', [MahasiswaController::class, 'createDataSubmit'])->name('data.addsubmit');

Route::get('/mahasiswa/{id}', [MahasiswaController::class, 'getdatabyid'])->name('data.getbyid');

Route::get('/hapus-data/{id}', [MahasiswaController::class, 'deleteData'])->name('data.delete');

Route::get('/editdata/{id}', [MahasiswaController::class, 'editData'])->name('data.edit');

Route::post('/updatedata', [MahasiswaController::class, 'updateData'])->name('data.update');