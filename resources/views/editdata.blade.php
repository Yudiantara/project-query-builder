<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div class="card">
                        <div class="card-header">
                            Edit Data
                            <a href="/mahasiswa" class="btn btn-info">Back</a>
                        </div>
                        <div class="card-body">
                            @if(Session::has('data_updated'))
                                <div class="alert alert-success" role="alert">
                                        {{Session::get('data_updated')}}
                                </div>
                            @endif
                            <form method="POST" action="{{route('data.update')}}">
                                @csrf
                                <input type="hidden" name="id" value="{{$data->id}}" />
                                <div class="form-group">
                                    <lable for="nama">Nama Mahasiswa :</lable>
                                    <input type="text" name="nama" class="form-control" value="{{$data->nama_mahasiswa}}" placeholder="Nama Mahasiswa" />
                                </div>
                                <div class="form-group">
                                    <lable for="nim">NIM :</lable>
                                    <input type="text" name="nim" class="form-control" value="{{$data->nim_mahasiswa}}" placeholder="Nama NIM Mahasiswa" />
                                </div>
                                <div class="form-group">
                                    <lable for="kelas">Kelas :</lable>
                                    <input type="text" name="kelas" class="form-control" value="{{$data->kelas_mahasiswa}}" placeholder="Nama Kelas Mahasiswa" />
                                </div>
                                <div class="form-group">
                                    <lable for="prodi">Prodi :</lable>
                                    <input type="text" name="prodi" class="form-control" value="{{$data->prodi_mahasiswa}}" placeholder="Nama Prodi" />
                                </div>
                                <div class="form-group">
                                    <lable for="fakultas">Fakultas :</lable>
                                    <input type="text" name="fakultas" class="form-control" value="{{$data->fakultas_mahasiswa}}" placeholder="Nama Fakultas" />
                                </div>
                                <input type="submit" class="btn btn-success" value="Update" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>
</html>