<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MahasiswaController extends Controller
{
    public function mahasiswa()
    {
        $data = DB::table('mahasiswa')->get();
        return view('mahasiswa', compact('data'));
    }

    public function createData()
    {
        return view('createData');
    }

    public function createDataSubmit(Request $request)
    {
        DB::table('mahasiswa')->insert([
            'nama_mahasiswa' => $request->nama,
            'nim_mahasiswa' => $request->nim,
            'kelas_mahasiswa' => $request->kelas,
            'prodi_mahasiswa' => $request->prodi,
            'fakultas_mahasiswa' => $request->fakultas
        ]);
        return back()->with('data_created','Data berhasil ditambahkan!');
    }

    public function getdatabyid($id)
    {
        $data = DB::table('mahasiswa')->where('id', $id)->first();
        return view('singledata', compact('data'));
    }

    public function deleteData($id)
    {
        DB::table('mahasiswa')->where('id', $id)->delete();
        return back()->with('data_deleted','Data berhasil dihapus!');
    }

    public function editData($id)
    {
        $data = DB::table('mahasiswa')->where('id', $id)->first();
        return view('editdata', compact('data'));
    }

    public function updateData(Request $request)
    {
        DB::table('mahasiswa')->where('id', $request->id)->update([
            'nama_mahasiswa' => $request->nama,
            'nim_mahasiswa' => $request->nim,
            'kelas_mahasiswa' => $request->kelas,
            'prodi_mahasiswa' => $request->prodi,
            'fakultas_mahasiswa' => $request->fakultas
        ]);
        return back()->with('data_updated','Data berhasil diedit!');
    }
}
